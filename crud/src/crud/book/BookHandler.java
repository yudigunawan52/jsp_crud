package crud.book;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BookHandler {

   
	private static String INSERT = "/addbook.jsp";
    private static String Edit = "/edit.jsp";
    private static String BookRecord = "/index.jsp";
    private BookDAO dao;

    public BookHandler() {
        super();
        dao = new BookDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String redirect="";
        String bId = request.getParameter("id");        
        String action = request.getParameter("action");
        if(!((bId)== null) && action.equalsIgnoreCase("insert"))
        {
        	int id = Integer.parseInt(bId);
        	BookBean book = new BookBean();
        	book.setId(id);
            book.setBook(request.getParameter("book"));
        	dao.insertBook(book);
        	redirect = BookRecord;
            request.setAttribute("book", dao.getAllBook());    
           	System.out.println("Record Added Successfully");
        }
        else if (action.equalsIgnoreCase("editform")){        	
        	redirect = Edit;            
        } else if (action.equalsIgnoreCase("edit")){
        	String boId = request.getParameter("id");
            int bobId = Integer.parseInt(boId);            
            BookBean book = new BookBean();
        	book.setId(bobId);
            book.setBook(request.getParameter("book"));
            dao.editBook(book);
            request.setAttribute("book", book);
            redirect = BookRecord;
            System.out.println("Record updated Successfully");
         } else if (action.equalsIgnoreCase("listBook")){
            redirect = BookRecord;
            request.setAttribute("book", dao.getAllBook());
         } else {
            redirect = INSERT;
        }

        RequestDispatcher rd = request.getRequestDispatcher(redirect);
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
