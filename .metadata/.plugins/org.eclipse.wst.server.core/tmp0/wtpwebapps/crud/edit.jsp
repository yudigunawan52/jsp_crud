<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="crud.book.BookBean"%>
<%@ page import="crud.book.BookDAO"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EDIT BOOK</title>
</head>
<body>
	<%
		BookBean book = new BookBean();
	%>
	<%
		BookDAO dao = new BookDAO();
	%>
	<form method="POST" action='BookHandler' name="frmEditBook"><input
		type="hidden" name="action" value="edit" /> <%
		String uid = request.getParameter("id");
		if (!((uid) == null)) {
		int id = Integer.parseInt(uid);
		book = dao.getBookById(id);
		%>
		<table>
			<tr>
				<td><input type="hidden" name="id" readonly="readonly"
				value="<%=book.getId()%>"></td>
			</tr>
			<tr>
				<td><input type="text" name="book" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Save" /></td>
			</tr>
		</table>
		
		<%
		} else
		out.println("ID Not Found");
		%>
	</form>
	<button href="BookHandler?action=listBook"></button>
</body>
</html>