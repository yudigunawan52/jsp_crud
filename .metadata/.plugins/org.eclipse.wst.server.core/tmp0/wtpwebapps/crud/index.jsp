<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="crud.book.BookBean"%>
<%@ page import="crud.book.BookDAO"%>
<%@ page import="java.util.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ALL BOOK</title>
</head>
<body>
	<%
		BookDAO dao = new BookDAO();
		List<BookBean> bookList = dao.getAllBook();
	%>
		<button><a href="BookHandler?action=insert">Add</a></button>
		<table border="1">
			<tr>
				<th>Id</th>
				<th>Book</th>
				<th>Action</th>
			</tr>
			<tr>
				<%
					for (BookBean book : bookList) {
				%>
				<td><%=book.getId()%></td>
				<td><%=book.getBook()%></td>
				<td><a href="BookHandler?action=editform&id=<%=book.getId()%>">Edit</a></td>
			
			
			</tr>
			<%
				}
			%>
	</table>
	
</body>
</html>